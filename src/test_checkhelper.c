/*
 Name        : test_checkhelper.c -- Test / demonstrates Helper header for Check unit test library
 Contributor : Eduardo A. do Valle Jr., 2014-01-27
 License     : LGPL 2.1 --- see http://check.sourceforge.net/COPYING.LESSER
 */

char usageString[] =
"usage: [test_executable] <case to fail> [verbosity flags: -(-q)uiet | -(-m)inimal | -(-n)ormal (default) | -(-v)erbose | -(-e)nvironment ]\n"
"       if -e or --environment get verbosity from environment variable CK_VERBOSITY (values: silent minimal normal verbose)\n";

#define CHECK_HELPER_MAIN_NAME checkMain
#define CHECK_HELPER_USAGE_STRING usageString

#include "checkhelper.h"


int failOnCase = -1; // Indicate the case in which we want to fail, for demonstration purposes

/* ----- Test cases ----- */


START_TEST (testCaseOne) {

    if (failOnCase == 0)
        checkAbort();

    checkAssert(failOnCase != 1);

    if (failOnCase == 2)
        checkAbortInfo("failed on case %d", failOnCase);

    checkAssertInfo(failOnCase != 3, "failed on case %d", failOnCase);

}
END_TEST


START_TEST (testCaseTwo) {

    int integer1=5, integer2=5;

    if (failOnCase == 4)
        integer2 = -integer2;
    checkAssertInt(integer1, ==, integer2);

    unsigned natural1=5, natural2=5;
    if (failOnCase == 5)
        natural2 = -natural2;
    checkAssertUInt(natural1, ==, natural2);

    if (failOnCase == 6)
        natural2 = -natural2;
    checkAssertXInt(natural1, ==, natural2);

    const char *text1 = "five";
    const char *text2 = "five";
    if (failOnCase == 7)
        text2 = "minus five";
    checkAssertString(text1, ==, text2);

}
END_TEST


START_TEST (testCaseThree) {

    const char *extra = "extra info !";

    int integer1=5, integer2=5;

    if (failOnCase == 8)
        integer2 = -integer2;
    checkAssertIntInfo(integer1, ==, integer2, "the numbers should be equal ! (%s)", extra);

    unsigned natural1=5, natural2=5;
    if (failOnCase == 9)
        natural2 = -natural2;
    checkAssertUIntInfo(natural1, ==, natural2, "the numbers should be equal ! (%s)", extra);

    if (failOnCase == 10)
        natural2 = -natural2;
    checkAssertXIntInfo(natural1, ==, natural2, "the numbers should be equal ! (%s)", extra);

    const char *text1 = "five";
    const char *text2 = "five";
    if (failOnCase == 11)
        text2 = "minus five";
    checkAssertStringInfo(text1, ==, text2, "the numbers should be equal ! (%s)", extra);

}
END_TEST



#ifdef checkAssertXXInt
START_TEST (testCaseFour) {

    const char *extra = "additional info !";

    unsigned __int128 natural1=5, natural2=5;
    if (failOnCase == 12)
        natural2 = -natural2;
    checkAssertXXInt(natural1, ==, natural2);

    if (failOnCase == 13)
        natural2 = -natural2;
    checkAssertXXIntInfo(natural1, ==, natural2, "the numbers should be equal ! (%s)", extra);
}
END_TEST
#endif


/* ----- Test suite ----- */


Suite * testSuite(void) {

    Suite *suite = suite_create("test checkhelper.c");

    TCase *testCase = tcase_create("Basic Functions");
    tcase_add_test(testCase, testCaseOne);
    suite_add_tcase(suite, testCase);

    testCase = tcase_create("Comparator Functions");
    tcase_add_test(testCase, testCaseTwo);
    suite_add_tcase(suite, testCase);

    testCase = tcase_create("Comparator Functions With Extra Info");
    tcase_add_test(testCase, testCaseThree);
    suite_add_tcase(suite, testCase);

    #ifdef checkAssertXXInt
        testCase = tcase_create("Integer-128 Functions");
        tcase_add_test(testCase, testCaseFour);
        suite_add_tcase(suite, testCase);
    #endif

    return suite;
}


/* ----- Main function ----- */

int main(int argc, char **argv) {

    // Parses from the command line which case to fail on purpose, otherwise pass all tests
    if  (argc>1) {
        char *end;
        failOnCase = strtol(argv[1], &end, 10);
        if (*end != '\0') {
            printf(usageString);
            return 1;
        }
    }
    else if (argc>1) {
        failOnCase = atoi(argv[1]);
    }

    if (argc == 1) {
        return CHECK_HELPER_MAIN_NAME(argc, argv);
    }
    else {
        argv[1] = argv[0];
        return CHECK_HELPER_MAIN_NAME(argc-1, argv+1);
    }
}



